import React, {Component} from 'react';
import Header from '../header'
import RandomPlanet from "../random-planet";
import ErrorIndicator from "../error-indicator";
import SwapiServices from "../../services/swapi-service";
import ErrorBoundary from "../error-boundary";
import {SwapiServiceProvider} from "../swapi-service-context";
import {PeoplePage, PlanetPage, StarshipPage, SecretPage, LoginPage} from '../pages';
import './app.css'

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {StarshipDetails} from "../sw-components";

class App extends Component {

    swapiServices = new SwapiServices();

    state = {
        hasError: false,
        isLoggedIn:false
    }

    onLogin = () => {
        this.setState({isLoggedIn: true});
    }

    componentDidCatch(error, errorInfo) {
        this.setState({
            hasError: true
        })
    }

    render() {

        if (this.state.hasError) return <ErrorIndicator/>;
        return (
            <ErrorBoundary>
                <Router>
                    <div className="app">
                        <SwapiServiceProvider value={this.swapiServices}>
                            <Header/>
                            <RandomPlanet/>
                            <Switch>
                            <Route path="/"
                                   render={() => <h1>Wellcome to the Starwars Db page</h1>}
                                   exact
                            />
                            <Route path="/people/:id?" component={PeoplePage}/>
                            <Route path="/people/" render={() => <h2>People page</h2>}/>
                            <Route path="/planets" component={PlanetPage}/>
                            <Route path="/starships" component={StarshipPage} exact/>
                            <Route path="/starships/:id"
                                   render={({match}) => {
                                       const {id} = match.params;
                                       return <StarshipDetails itemId={id}/>

                                   }}/>

                            <Route path="/login" render = { () => (
                                <LoginPage
                                    isLoggedIn={this.state.isLoggedIn}
                                    onLogin={this.onLogin}/>
                            )}/>
                            <Route path="/secret" render = { () => (
                                <SecretPage isLoggedIn={this.state.isLoggedIn}/>
                            )}/>
                                <Route render={() => <h2>404! Page not found</h2>}/>
                            </Switch>
                        </SwapiServiceProvider>
                    </div>
                </Router>
            </ErrorBoundary>
        )
    }

}

export default App;