import React from "react";
import './header.css'
import {Link} from 'react-router-dom';

const Header = () => {
    return(
        <div className="header">
            <h3>
                <Link to="/"> Starwars DB</Link>
            </h3>
            <ul>
                <li>
                    <Link to="/people/">People</Link>
                </li>
                <li>
                    <Link to="/planets/">Planets</Link>
                </li>
                <li>
                    <Link to="/starships/">Starship</Link>
                </li>
                <li>
                    <Link to="/login">Login</Link>
                </li>
                <li>
                    <Link to="/secret">Secret</Link>
                </li>
            </ul>
        </div>
    )
}

export default Header