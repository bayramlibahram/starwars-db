import React, {Component} from "react";
import Spinner from "../spinner";

const withData = (View) => {
    return class extends Component {
        state = {
            data: null,
        }

        componentDidMount() {
            this.props.getData()
                .then((data) => {
                    this.setState({data})
                })
                .catch((err) => console.error(err))
        }

        render() {

            const {data} = this.state;

            if (!data) return <Spinner/>;
            console.log('this props with data', {...this.props});
            return <View {...this.props} data={data}/>
        }
    }
}

export default withData;