import React from 'react';
import {SwapiServiceConsumer} from "../swapi-service-context";

const withSwapiService = (Wrapped, mapMethodsToProps) => {
    return (props) => {
        return (
            <SwapiServiceConsumer>
                {
                    (swapiServices) => {
                        const serviceProps = mapMethodsToProps(swapiServices);
                        return <Wrapped {...props} {...serviceProps}/>
                    }
                }
            </SwapiServiceConsumer>
        )
    }
}

export default withSwapiService;