import React, {Component} from 'react';
import './item-details.css';
import Spinner from "../spinner";
import ErrorButton from "../error-button";

const Record = ({item, field, label}) => {
    return (
        <li className="list-group-item">
            <span className="term">{label}</span>
            <span>{item[field]}</span>
        </li>
    )
}

export {
    Record
}

export default class ItemDetails extends Component {

    state = {
        item: null,
        image: null
    }

    componentDidMount() {
        this.updateItem()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.itemId !== this.props.itemId) {
            this.updateItem();
            this.setState({item: null})
        }
    }

    updateItem = () => {
        const {itemId, getData, getImageUrl} = this.props;

        if (!itemId) return;
        getData(itemId)
            .then((item) => {
                this.setState({
                    item,
                    image: getImageUrl(item)
                })
            })
    }

    render() {

        if (!this.props.itemId) return <span>Select person from a list</span>;

        const {item, image} = this.state;

        if (!item) return <Spinner/>;

        const {name} = item;

        return (
            <div className="person-details card">
                <img
                    className="person-image"
                    src={image}
                    alt="img"
                />
                <div className="card-body">
                    <h4>{name} </h4>
                    <ul className="list-group list-group-flush">
                        {
                            React.Children.map(this.props.children, (child) => {
                                return React.cloneElement(child, {item})
                            })
                        }
                    </ul>
                    <ErrorButton/>
                </div>
            </div>
        )
    }

}
