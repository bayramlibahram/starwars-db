import React from 'react';
import './item-list.css';

const returnData = (data, onItemSelected, renderFn) => {
       return  data.map((item) => {
           const label = renderFn(item);
           return (
               <li
                   key={item.id}
                   className="list-group-item"
                   onClick={() => onItemSelected(item.id)}
               >
                   {label}
               </li>
           )
       })
}

const ItemList = (props) => {

    const {data, onItemSelected, children: renderLabel} = props;

    const list = returnData(data, onItemSelected, renderLabel);

    return (
        <ul className="item-list list-group">
            {list}
        </ul>
    );

}

export default ItemList;
