import PeoplePage from "./people-page";
import PlanetPage from "./planet-page";
import StarshipPage from "./starship-page";
import SecretPage from "./secret-page";
import LoginPage from "./login-page.jsx";
export {
    PeoplePage,
    PlanetPage,
    StarshipPage,
    SecretPage,
    LoginPage
}