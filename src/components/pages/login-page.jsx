import React from 'react';
import {Redirect} from "react-router-dom";
const LoginPage = ({isLoggedIn, onLogin}) => {
    if(isLoggedIn) {
       return <Redirect to="/secret"/>
    }

    return (
        <div className="jumbotron text-center">
            <h2>Login the secret page !</h2>
            <button className="btn btn-primary btn-rounded" onClick={onLogin}>Login</button>
        </div>
    )
}

export default LoginPage;