import React from 'react';
import {StarshipList} from "../sw-components";
import {withRouter} from "react-router-dom";
// export default class StarshipPage extends Component {
//
//     state = {
//         selectedItem: null
//     }
//
//     onItemSelected = (selectedItem) => {
//         this.setState({
//             selectedItem
//         })
//     }
//
//     render() {
//         return (
//             <Row
//                 left={<StarshipList onItemSelected={this.onItemSelected}/>}
//                 right={<StarshipDetails itemId={this.state.selectedItem}/>}
//             />
//         )
//     }
//
// }

const StarshipPage = ({history}) => {
    return (
        <StarshipList onItemSelected={(itemId) =>history.push(`${itemId}`)} />
    )
}

export default withRouter(StarshipPage);