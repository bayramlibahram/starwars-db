import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Spinner from '../spinner';
import SwapiServices from "../../services/swapi-service";
import './random-planet.css';
import ErrorIndicator from "../error-indicator";



export default class RandomPlanet extends Component {

    static defaultProps = {
        updateInterval: 10000
    }

    // static propTypes = {
    //     updateInterval: (props, propName, componentName) => {
    //         const value = props[propName];
    //         if (value === 'number' && !isNaN(value)) {
    //             return null;
    //         }
    //         return new TypeError(`${componentName} :${propName} must be a number`);
    //     }
    // }

    static propTypes = {
        updateInterval: PropTypes.number
    }

    swapiServices = new SwapiServices();

    state = {
        planet: {},
        loading: true,
        error: false
    }

    componentDidMount() {
        const {updateInterval} = this.props;
        this.updatePlanet();
        this.interval = setInterval(this.updatePlanet, updateInterval)
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    onPlanetLoaded = (planet) => {
        this.setState({
            planet,
            loading: false
        })
    }

    onError = (error) => {
        this.setState({
            loading: false,
            error: true
        })
    }

    updatePlanet = () => {
        console.log('update ---')
        const id = Math.round(Math.random() * 9)
        this.swapiServices
            .getPlanet(id)
            .then(this.onPlanetLoaded)
            .catch(this.onError)
    }

    render() {
        console.log('render ---')
        const {planet, loading, error} = this.state;

        const hasData = !(loading || error);

        const spinner = loading ? <Spinner/> : null;
        const errorMessage = error ? <ErrorIndicator/> : null;
        const content = hasData ? <PlanetView planet={planet}/> : null;

        return (
            <div className="random-planet jumbotron rounded mb-2">
                {errorMessage}
                {spinner}
                {content}
            </div>
        );
    }

}

const PlanetView = ({planet}) => {
    const {id, name, population, rotationPeriod, diameter} = planet;
    return (
        <React.Fragment>
            <img className="planet-image"
                 src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`}
                 alt={name}
            />
            <div>
                <h4>{name}</h4>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                        <span className="term">Population</span>
                        <span>{population}</span>
                    </li>
                    <li className="list-group-item">
                        <span className="term">Rotation Period</span>
                        <span>{rotationPeriod}</span>
                    </li>
                    <li className="list-group-item">
                        <span className="term">Diameter</span>
                        <span>{diameter}</span>
                    </li>
                </ul>
            </div>
        </React.Fragment>
    );
}
