import React from 'react';
import './spinner.css'

const Spinner = () => {
    return (
        <div className="loadingio-spinner-ripple-eihv2danex">
            <div className="ldio-8nt3au8vrdq">
                <div></div>
                <div></div>
            </div>
        </div>
    )
}

export default Spinner;