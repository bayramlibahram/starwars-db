import React from 'react';
import {withData, withSwapiService} from '../hoc-helpers';
import ItemList from "../item-list";
import PropTypes from "prop-types";

const withChildFunction = (Wrapped, fn) => {
    return (props) => {
        return (
            <Wrapped {...props}>
                {fn}
            </Wrapped>
        )
    }
}

const renderName = ({name}) => <span>{name}</span>;

const mapPersonMethodsToProps = (swapiService) => {
    return {
        getData: swapiService.getAllPlanet
    }
}

const mapPlanetMethodsToProps = (swapiService) => {
    return {
        getData: swapiService.getAllPlanet
    }
}

const mapStarShipMethodsToProps = (swapiService) => {
    return {
        getData: swapiService.getAllStarships
    }
}

const PersonList = withSwapiService(
    withData(
        withChildFunction(ItemList, renderName)
    ), mapPersonMethodsToProps);

const PlanetList = withSwapiService(withData(
    withChildFunction(ItemList, renderName),
), mapPlanetMethodsToProps);

const StarshipList = withSwapiService(withData(
    withChildFunction(ItemList, renderName),
), mapStarShipMethodsToProps);


ItemList.defaultProps = {
    onItemSelected:() => {}
}

ItemList.propTypes = {
    onItemSelected: PropTypes.func,
    data:PropTypes.arrayOf(PropTypes.object).isRequired,
    children: PropTypes.func
}

export {
    PersonList,
    PlanetList,
    StarshipList
}