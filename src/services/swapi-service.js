export default class SwapiServices {

    _baseUrl = 'https://swapi.dev/api';

    _imageBase = 'https://starwars-visualguide.com/assets/img/';

    getResource = async (url) => {
        const res = await fetch(`${this._baseUrl}${url}`);
        if (!res.status) throw new Error(`Could not find ${this._baseUrl}${url}, received ${res.status}`);
        const body = res.json();
        return body
    }

    getAllPeople = async () => {
        const res = await this.getResource('/people/')
        return res.results.map(person => this._transformPerson(person));
    }

    getPerson = async (id) => {
        const person = await this.getResource(`/people/${id}/`);
        return this._transformPerson(person);
    }

    getAllPlanet = async () => {
        const res = await this.getResource('/planets/');
        return res.results.map(planet => this._transformPlanet(planet));
    }

    getAllStarships = async () => {
        const starships = await this.getResource('/starships/');
        return starships.results.map(starship => this._transformStarship(starship));
    }

    getStarship = async (id) => {
        const starships = await this.getResource(`/starships/${id}`)
        return this._transformStarship(starships)
    }

    getPlanet = async (id) => {
        const planet = await this.getResource(`/planets/${id}`)
        return this._transformPlanet(planet)
    }

    getPersonImage = ({id}) => {
        return `${this._imageBase}/characters/${id}.jpg`
    }

    getPlanetImage = ({id}) => {
        return `${this._imageBase}/planets/${id}.jpg`
    }

    getStarshipImage = ({id}) => {
        return `${this._imageBase}/starships/${id}.jpg`
    }

    _extractId = (item) => {
        const regExp = /\/([0-9]*)\/$/
        const id = item.url.match(regExp)[1];
        return id
    }

    _transformPlanet = (planet) => {
        const res = {
            id: this._extractId(planet),
            name: planet.name,
            population: planet.population,
            rotationPeriod: planet.rotation_period,
            diameter: planet.diameter,
        }
        return res
    }

    _transformStarship = (starship) => {
        return {
            id: this._extractId(starship),
            name: starship.name,
            model: starship.model,
            manufacturer: starship.manufacturer,
            costInCredits: starship.costInCredits,
            length: starship.length,
            crew: starship.crew,
            passenger: starship.passenger,
            cargoCapacity: starship.cargoCapacity,
        }
    }

    _transformPerson = (person) => {
        return {
            id: this._extractId(person),
            name: person.name,
            gender: person.gender,
            birthYear: person.birth_year,
            eyeColor: person.eye_color,
        }
    }
}


